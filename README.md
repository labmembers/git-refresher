# Git Refresher Workshop

Welcome to the Git Refresher Workshop repository! This repository contains materials for a mini-workshop on how to use repositories to manage your research and share resources within your lab.

All content is available on the [repository wiki](https://bitbucket.org/labmembers/git-refresher/wiki/Home).

Written by Daniel Feuerriegel 4/18